﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbarrotesElMirador.Data;
using AbarrotesElMirador.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AbarrotesElMirador.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : Controller
    {
        private MiradorDbContext _miradorDbContext;

        public ProductosController(MiradorDbContext miradorDbContext)
        {
            _miradorDbContext = miradorDbContext;
        }

        // GET:
        [HttpGet("ObtenerProductos")]
        public ProductoListResponse ObtenerProductos()
        {
            List<Productos> list = new List<Productos>();
            var response = new ProductoListResponse();
            try
            {
                list = _miradorDbContext.Productos.ToList();
                if (list.Count != 0)
                {
                    response.Estatus = true;
                    response.Mensaje = "Productos obtenidos con éxito.";
                    response.ListaProductos = list;
                }
                else
                {
                    response.Estatus = false;
                    response.Mensaje = "No se han encontrado productos.";
                    response.ListaProductos = list;
                }
            }
            catch (Exception ex)
            {
                response.Estatus = false;
                response.Mensaje = "Error!" + ex.Message;
            }
            return response;
        }

        // GET:
        [HttpGet("ObtenerDatosProducto")]
        public ProductoResponse ObtenerDatosProducto(string codigoBarra)
        {
            List<Productos> list = new List<Productos>();
            var response = new ProductoResponse();
            if (codigoBarra != null)
            {
                try
                {

                    var product = from v in _miradorDbContext.Productos
                                  where v.CodigoBarra == codigoBarra
                                  select new Productos
                                  {
                                      Id = v.Id,
                                      NombreProducto = v.NombreProducto,
                                      CodigoBarra = v.CodigoBarra,
                                      Precio = v.Precio,
                                      Tipo = v.Tipo,
                                      FechaCaducidad = v.FechaCaducidad,
                                  };
                    list = product.ToList();

                    if (list.Count != 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            response.Id = list[i].Id;
                            response.NombreProducto = list[i].NombreProducto;
                            response.CodigoBarra = list[i].CodigoBarra;
                            response.Precio = list[i].Precio;
                            response.Tipo = list[i].Tipo;
                            response.FechaCaducidad = list[i].FechaCaducidad;
                        }
                        response.Estatus = true;
                        response.Mensaje = "Producto obtenido";
                    }
                    else
                    {
                        response.Estatus = false;
                        response.Mensaje = "No se han encontrado el producto.";
                    }
                }

                catch (Exception ex)
                {
                    response.Estatus = false;
                    response.Mensaje = "Error!" + ex.Message;
                }
            }
            else
            {
                response.Estatus = false;
                response.Mensaje = "No se envio el codigo de barras.";
            }
            return response;
        }
    }
}

﻿using System;
namespace AbarrotesElMirador.Models
{
    public class ResponseBase
    {
        public bool Estatus { get; set; }
        public string Mensaje { get; set; }
        public ResponseBase()
        {
        }
    }
}

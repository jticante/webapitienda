﻿using System;
using System.Collections.Generic;

namespace AbarrotesElMirador.Models
{
    public class ProductoListResponse: ResponseBase
    {
        public List<Productos> ListaProductos { get; set; }
        public ProductoListResponse()
        {
        }
    }
}

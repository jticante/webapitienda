﻿using System;
namespace AbarrotesElMirador.Models
{
    public class ProductoResponse : ResponseBase
    {
        public int Id { get; set; }
        public String NombreProducto { get; set; }
        public String CodigoBarra { get; set; }
        public Double Precio { get; set; }
        public String Tipo { get; set; }
        public DateTime FechaCaducidad { get; set; }
        public ProductoResponse()
        {
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AbarrotesElMirador.Models
{
    public class Productos
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public String NombreProducto { get; set; }

        [Required]
        [StringLength(100)]
        public String CodigoBarra { get; set; }

        [Required]
        public Double Precio { get; set; }

        [Required]
        public String Tipo { get; set; }

        [Required]
        public DateTime FechaCaducidad { get; set; }

        public Productos()
        {
        }
    }
}

﻿using System;
using AbarrotesElMirador.Models;
using Microsoft.EntityFrameworkCore;

namespace AbarrotesElMirador.Data
{
    public class MiradorDbContext: DbContext
    {

        public MiradorDbContext(DbContextOptions<MiradorDbContext> options) : base(options)
        {
        }
        public DbSet<Productos> Productos { get; set; }
    }
}
